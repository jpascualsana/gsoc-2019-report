# Import public datasets on RetroShare network & create self-generated API wrapper for RetroShare JSON API
#### Gsoc-2019-report

## Introduction

Greetings to GSoC community!

This was an intense GSoC, on particular on our project we had lots of changes:
we started with the project of [import public datasets on a RetroShare network](https://gitlab.com/jpascualsana/public-datasets-import)
using the JSON API. The objective of this was create **distributed
repositories** of information that already exists on centralized servers.
Also with the idea to **create bridges between internet and the RetroShare
network**.

But to do that we needed to develop an API wrapper that talks with the
RetroShare backend. Talking with my mentor we start to develop a [python script](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator.py)
that **analyzing the Doxygen XML files generate an API wrapper automatically**.
The hard work here was to analyze how Doxygen save the information of the
RetroShare classes, endpoints... And parse the RsClasses to a primitive types or
compund classes.

After we get the auto-generated wrapper we decided to focus our development to
give support to the [OpenAPI](https://pad.riseup.net/redirect#https%3A//swagger.io/specification/)
specification. We modified the previous [api-wrapper-generator](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator.py)
, that generates a python wrapper, to **generate a [YAML](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator.py)
that describe the RetroShare API on an OpenAPI Specification**. Thanks to this
generated YAML we can use now the [openapi-generator project](https://pad.riseup.net/redirect#https%3A//github.com/openapitools/openapi-generator/) to **generate a client API wrapper for RetroShare JSON API on any
language** supported by the generator project.

#### In conclusion

**We didn't finish the project of public datasets import** because we
decided to give more importance to the auto-generated API wrappers using the mix
of generated Doxygen documentation and the OpenAPI Generator Project.

So, the workflow to generate an API wrapper, should be:

1. Compile the RetroShare desired commit to generate the Doxygen documentation.
2. Use the [jsonapiwrapper-generator-openapi.py](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator-openapi.py)
script that generates the [OpenAPI YAML](https://gitlab.com/snippets/1882068)
specification for RetroShare API.
3. Generate the frontend code using the OpenAPI Generator Project on to the
desired language. For example, you can check the result for python on this
[example](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper)

Following this steps, easily you can have an updated RetroShare API wrapper
**without coding anything!**.

## Repositories

#### [RetroShare public datasets import](https://gitlab.com/jpascualsana/public-datasets-import)

We develop a series of command line scripts that are able to get information
from public datasets and present it ordered prepared to be imported to the RS
network.

Some examples of supported platforms:

* Wikimedia based projects
* Wordpress blogs
* Gutemberg project
* ActivityPub
* RSS
* Radio onda Rossa
* XRCB.cat
* RadioTeca

You can use `--help` to see how each of this scripts works.

![ActivityPub help](actiitypub_help.png)

After develop this clients that get the information from the internet we had to
develop the way to import all of this data on to the RetroShare decentralized
network. To do that we decided to create an **automatically generated wrapper**
using the Doxygen documentation generated on RetroShare build time.

#### RetroShare automatic API wrapper generation

##### [Python API wrapper](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator)

The script of [`jsonapiwrapper-generator.py`](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator.py) get the Doxygen documentation path as parameter. Then starts to
parse the files that contain the endpoints and recursively the RetroShare
classes used on each call.

```bash
python3 jsonapiwrapper-generator.py <build-RetroShare-desktop_qt5-Debug/jsonapi-generator/src>
```

It generate two files:

* [`wrapper_python.py`](https://gitlab.com/snippets/1877207): this wrapper
contain classes that store the API calls.
* [`wrapper_class_python.py`](https://gitlab.com/snippets/1875153): this
file contain all the RetroShare classes, divided on two groups:
  1. Compound classes, tipically C++ structs that contain primitive types or
  other RsClasses. For example [RsGxsForumGroup](https://gitlab.com/snippets/1875153#L5).
  2. Typedef classes, this ones that are recursively parsed until find it
  primitive type. For example [RsGxsGroupId](https://gitlab.com/snippets/1875153#L51)

The script generate this files using a templates located on `tmpl/` folder. So
it should be not very difficult to generate wrappers for other languages. You
can find instructions of how [here](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator#how-to).

Actually this script supports:

* Document the code using DocString convention
* Parse also 'manualwrappers' like attempt login
* Tested requests with authentication and without
* Support basic authorization or token auth via 'Authentication: basic bas64Token' header.
* Async methods support and callback implementation.
* RsClass parsed

You can find some tests [here](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/test_all.py).

![Async methods test](test_async_methods.png)

##### [OpenAPI specification support](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper)

With the last step we learned how Doxygen generate the files, how to parse it
and how to transform it into an automatic generated wrapper using templates.
Talking with my mentor we realize of the importance to create self generated
wrappers in multiple languages for the developers that wants to code apps over
RetroShare network using JSON API. We noticed about OpenAPI specification,
defined by the [Wikipedia](https://en.wikipedia.org/wiki/OpenAPI_Specification)
as:

> The OpenAPI Specification, is a specification for machine-readable interface
files for describing, producing, consuming, and visualizing RESTful web services.

The most interesting part for us was the [OpenAPI Generator Project](https://github.com/openapitools/openapi-generator/)
. Is a tool-set able to generate any OpenAPI YAML to a client for it
specification on a sets of [different languages](https://openapi-generator.tech/docs/generators).
Briefly: generate a client for a defined backend in one line command.

So we adapt the `jsonapiwrapper-generator.py` to generate a YAML that
define the RetroShare backend with OpenAPI specification. The resulting script
was [`jsonapiwrapper-generator-openapi.py`](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/blob/master/jsonapiwrapper-generator-openapi.py) and generate an YAML called: [`wrapper_openapi.yml`](https://gitlab.com/snippets/1882068)
that contain the same information as the previous wrapper but with OpenAPI
specification. So, if we want to generate an API wrapper for python we can just:

```bash
openapi-generator generate -i wrapper_openapi.yml -o RS_WRAPPER_DIR -g python
```

* [Tests for unauthenticated methods](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/blob/master/test/test_req_json_api_server_version.py#L31)

![](test_unauth.png)

* [Tests for login](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/blob/master/test/test_rs_login_helper_location.py#L32)

![](test_login_helper.png)

* [Tests for authenticated end points](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/blob/master/test/test_req_rs_gxs_channels_create_channel_v2.py#L32)

![](test_auth_methods.png)

* [Tests for models](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/blob/master/test/test_req_rs_gxs_channels_create_channel.py#L33): when the model objects are created using the OpenAPI Generator, you can pass an entire object as dictionary or instantiate it with the provided Class created by the generator. For example, on the link above we can see how to instantiate the object instead of pass it as dictionary.

```python
groupMetada = RsGroupMetaData(m_group_name="Just another test channel2", m_group_flags=4, m_sign_flags=520)
channel = RsGxsChannelGroup(m_meta=groupMetada, m_description="Test it!")
req_rs_gxs_channels_create_channel = {"channel": channel}
```

![](test_models.png)

* [Tests for publish post with file upload](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/commit/39ff028c4a4c29aac39595f98e6d43cfd3b4b046): look at the [commit](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper/commit/39ff028c4a4c29aac39595f98e6d43cfd3b4b046)
, first the file should be on a shared directory to be shared properly. 

![](channel_creation.png)

After pass the generated YAML to the openapi-generator validator we found lots
of bugs and improvements for the parsing script. See [#3](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/issues/3).

## TODO's

And the end of this work we didn't have enough time to implement the wrappers to
the `public-datasets-import` project, so I would like to finish this integration
on the following months.

* [ ] Integrate the [OpenAPI generated wrapper](https://gitlab.com/jpascualsana/openapi-python-retroshare-api-wrapper) to [public-datasets-import](https://gitlab.com/jpascualsana/public-datasets-import)
project.

## Repositories

* [public-datasets-import](https://gitlab.com/jpascualsana/public-datasets-import)
* [retroshare-api-wrapper-generator](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator)
* [openapi-python-retroshare-api-wrapper](https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator)

## Freifunk posts

1. [Arrival](https://blog.freifunk.net/2019/05/26/library-to-export-import-public-datasets-to-retroshare-network/)
2. [First evaluation](https://blog.freifunk.net/2019/06/24/gsoc-2019-import-public-datasets-to-retroshare-network-first-evalutaion/)
3. [Second evaluation](https://blog.freifunk.net/2019/07/21/gsoc-2019-import-public-datasets-to-retroshare-network-second-evalutaion/)
4. [Third evaluation](https://blog.freifunk.net/2019/08/24/gsoc-2019-import-public-datasets-to-retroshare-network-final-evalutaion/)
